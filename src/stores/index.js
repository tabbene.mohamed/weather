import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import RWeather from '../reducers/index';
const store = createStore(RWeather, applyMiddleware(thunkMiddleware));
export default store;
