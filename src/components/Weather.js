import React,{useState,useEffect}  from "react";
import { useDispatch, useSelector } from 'react-redux';
import {fetchWeatherOneCountry,fetchWeatherCountryXday} from '../actions/index';
import Forecastdaydetails from './Forecastdaydetails';


const Weather = () => {
    const dispatch = useDispatch();
    const [namecountry, setNamecountry] = useState('');
    const [location, setLocation] = useState('');
    const [current, setCurrent] = useState('');
    const [forecast, setForecast] = useState('');
    const [namexdaycountry, setNamexdaycountry] = useState('');
    const [numberxday, setNumberxday] = useState(2);
    const weartheronecountry = useSelector((state) => state._manageWeatherState.weatherOneCountry);
    const wearthercountryxday = useSelector((state) => state._manageWeatherState.weatherCountryXday);


    useEffect(() => {
        setLocation(weartheronecountry.location);
        setCurrent(weartheronecountry.current)
        // eslint-disable-next-line react-hooks/exhaustive-deps
      }, [weartheronecountry]);

      useEffect(() => {
        setForecast(wearthercountryxday.forecast)
        // eslint-disable-next-line react-hooks/exhaustive-deps
      }, [wearthercountryxday]);
    
  

   const wearther = () => {
    dispatch(fetchWeatherOneCountry(namecountry));
   }
   const weartherXday = () => {
    dispatch(fetchWeatherCountryXday(namexdaycountry,numberxday));
   }

    return(
    <div className="container">
<h1>Weather Country</h1>
  <div className="col-auto">
    <label>Name Country</label>
    <input type="text" value={namecountry} onChange={(e)=>setNamecountry(e.target.value)}  className="form-control" />
  </div>
  <div className="col-auto">
    <button className="btn btn-primary mb-3 mt-3" onClick={wearther} >submit</button>
  </div>
  <div className="row">
  {current && <div className="card">
  <img src={current.condition.icon} className="card-img-top" alt="icon"/>
  <div className="card-body">
    <p>{current.condition.text}</p>
    <b>{current.cloud}</b>
  </div>
</div>
  }
  {location &&
  <ul>
            <li>country : {location.country}</li>
            <li>name : {location.name}</li>
            <li>region : {location.region}</li>
            <li>localtime : {location.localtime}</li>
            </ul>
  }

  </div>
  <h1>Weather Country X day</h1>
  <div className="col-auto">
    <label>Name Country</label>
    <input type="text" value={namexdaycountry} onChange={(e)=>setNamexdaycountry(e.target.value)}  className="form-control" />
  </div>
  <div className="col-auto">
    <label>Day</label>
    <input type="number" value={numberxday} onChange={(e)=>setNumberxday(e.target.value)}  className="form-control" />
  </div>
  <div className="col-auto">
    <button className="btn btn-primary mb-3 mt-3" onClick={weartherXday} >submit</button>
  </div>
  {forecast &&  forecast.forecastday.map((forecastday, index) => (
    <div key={index} className="row">
    <h4>{forecastday.date}</h4>
    <Forecastdaydetails forecastdayhour={forecastday.hour} />
    </div>
  ))
  }
</div>
 )
}
export default Weather;
