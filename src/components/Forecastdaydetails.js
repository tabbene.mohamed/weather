import React  from "react";


const Forecastdaydetails = ({forecastdayhour}) => {

return(
    <div className="table-responsive">
<table className="table">
    <tbody>
    <tr>
    {forecastdayhour.map((forecastday, index) => (
   <td key={index}>
   <div className="card">
    <small>{forecastday.time}</small>
  <img src={forecastday.condition.icon} className="card-img-top" alt="icon"/>
  <div className="card-body">
    <p>{forecastday.condition.text}</p>
    <b>{forecastday.cloud}</b>
  </div>
</div>
   </td>
  ))
  }
    </tr>
    </tbody>
</table>
</div>
)
}
export default Forecastdaydetails;