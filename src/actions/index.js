import callApi from '../api';
import * as type from '../types';

 export const fetchWeatherOneCountry = (country) => {
   return (dispatch) => {
     return callApi(`current.json?key=${process.env.REACT_APP_API_KEY}&q=${country}&aqi=no`, 'GET', null).then(
       (res) => {
         dispatch(GetAllWeatherOneCountry(res.data));
       }
     );
   };
 };
 
 export const fetchWeatherCountryXday = (country,day) => {
    return (dispatch) => {
      return callApi(`forecast.json?key=${process.env.REACT_APP_API_KEY}&q=${country}&days=${day}&aqi=no&alerts=no`, 'GET', null).then(
        (res) => {
          dispatch(GetAllWeatherCountryXday(res.data));
        }
      );
    };
  };


 export const GetAllWeatherOneCountry = (payload) => ({
   type: type.GET_ALLWEATHERONECOUNTRY,
   payload,
 });

 export const GetAllWeatherCountryXday = (payload) => ({
    type: type.GET_ALLWEATHERCOUNTRYXDAY,
    payload,
  });
 