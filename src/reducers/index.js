import { combineReducers } from 'redux';
import * as type from '../types';

const initWeatherState = {
  weatherOneCountry:{},
  weatherCountryXday:{}
};

const RWeather = (state = initWeatherState, action) => {
  switch (action.type) {
    case type.GET_ALLWEATHERONECOUNTRY:
      return {
        ...state,
        weatherOneCountry : action.payload,
      };
      case type.GET_ALLWEATHERCOUNTRYXDAY:
        return {
          ...state,
          weatherCountryXday : action.payload,
        };
    default:
      return state;
  }
};
const Weather = combineReducers({
  _manageWeatherState: RWeather,
});
export default Weather;
